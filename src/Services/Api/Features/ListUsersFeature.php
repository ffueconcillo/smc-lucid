<?php
namespace App\Services\Api\Features;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;

use App\Domains\User\Jobs\GetUsersJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;

class ListUsersFeature extends Feature
{
    public function handle(Request $request)
    {
        $users = $this->run(GetUsersJob::class);
        
        return $this->run(new RespondWithJsonJob($users));
    }
}
