<?php
namespace App\Services\Api\Features;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\User\Jobs\LogEmptySearchResultsJob;
use App\Domains\User\Jobs\LogUserSearchJob;
use App\Domains\User\Jobs\SearchUsersByNameJob;
use App\Domains\User\Jobs\ValidateUserSearchQueryJob;


class SearchUsersFeature extends Feature
{
    public function handle(Request $request)
    {
        // validate input - if not valid the validator should
        // throw an exception of InvalidArgumentException
        $this->run(new ValidateUserSearchQueryJob($request->input()));
        
        $results = $this->run(SearchUsersByNameJob::class, [
            'query' => $request->input('query'),
        ]);
    
        if (empty($results)) {
            $this->run(LogEmptySearchResultsJob::class, [
                'date' => new \DateTime(),
                'query' => $request->query(),
            ]);
    
            $response = $this->run(new RespondWithJsonErrorJob('No users found'));
        } else {
            // this job is queueable so it will automatically get queued
            // and dispatched later.

            // - has issues on DateTime
            // $this->run(LogUserSearchJob::class, [
            //     'date' => new \DateTime(),
            //     'query' => $request->input(),
            //     'results' => $results->pluck('id'), // only the ids of the results are required
            // ]);
    
            $response = $this->run(new RespondWithJsonJob($results));
        }
    
        return $response;

    }
}
