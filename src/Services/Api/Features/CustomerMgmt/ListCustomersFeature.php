<?php
namespace App\Services\Api\Features\CustomerMgmt;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;

use App\Domains\Customer\Jobs\ListCustomersJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;



class ListCustomersFeature extends Feature
{
    public function handle(Request $request)
    {
        $customers = $this->run(ListCustomersJob::class);
        
        return $this->run(new RespondWithJsonJob($customers));

    }
}
