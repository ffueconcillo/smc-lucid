<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ListUsersFeature;
use App\Services\Api\Features\SearchUsersFeature;

class UserController extends Controller
{
    /**
     * @SWG\Info(
     *   title="SMC-Lucid API Documentation", 
     *   version="1.0",
     *   description="SMC-Lucid API v1.0"
     * )
     * @SWG\Tag(name="Customer")
     * @SWG\Tag(name="User")
     */

    /**
     * @SWG\Get(
     *     path="/api/v1/user",
     *     tags={"User"},
     *     @SWG\Response(response="200", description="List users.")
     * )
     * 
     */
    public function index()
    {
        return $this->serve(ListUsersFeature::class);
    }

    /**
     * @SWG\Get(
     *   path="/api/v1/user/search?query={query}",
     *   summary="Search a user by name",
     *   tags={"User"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="query",
     *     type="string"
     *   ),
     *   @SWG\Response(response="200", description="Search a user by name")
     * )
     */
    public function search()
    {
        return $this->serve(SearchUsersFeature::class);
    }
}
