<?php
namespace App\Domains\User\Jobs;

use Datetime;
use Lucid\Foundation\Job;

class LogUserSearchJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($query, array $resultIds, DateTime $date)
    {
        //
    }
}
