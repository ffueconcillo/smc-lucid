<?php
namespace App\Domains\Customer\Jobs;

use Lucid\Foundation\Job;

use App\Data\Customer;

class ListCustomersJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($limit = 25)
    {
        $this->limit = $limit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Customer $customer)
    {
        return $customer->take($this->limit)->get();
    }
}
