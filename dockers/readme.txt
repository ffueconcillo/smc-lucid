Run apache-php7

// build api image
cd /dockers/api
docker build -t api .

// Run containers
docker run -d -p 8000:8000 -v $(pwd):/var/www -w /var/www --name lucid ilovintit/php7.1-apache
docker run --name mysql -e MYSQL_ROOT_PASSWORD=Password1 -d mysql/mysql-server:latest