<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'francis',
            'email' => 'francisfueconcillo@gmail.com',
            'password' => bcrypt('Password1'),
        ]);

        DB::table('users')->insert([
            'name' => 'john',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('Password1'),
        ]);
    }
}
