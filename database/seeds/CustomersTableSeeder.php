<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'company_name' => 'Company A',
            'email' => 'company_a@gmail.com',
            'tag_id' => 'COM_C',
        ]);

        DB::table('customers')->insert([
            'company_name' => 'Company B',
            'email' => 'company_b@gmail.com',
            'tag_id' => 'COB_C',
        ]);
    }
}
